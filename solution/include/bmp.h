#ifndef BMP_H
#define BMP_H

#include "image.h"

#include <stdio.h>

#define BMP_BF_TYPE  0x4D42
#define BMP_BF_RESERVED  0
#define BMP_BO_OFF_BITS  54
#define BMP_BI_SIZE  40
#define BMP_FILE_HEADER_SIZE  14
#define BMP_BI_PLANES  1
#define BMP_BI_BIT_COUNT  24
#define BMP_BI_COMPRESSION  0
#define BMP_BI_X_PELS_PER_METER  2835
#define BMP_BI_Y_PELS_PER_METER  2835
#define BMP_BI_CLR_USED  0
#define BMP_BI_CLR_IMPORTANT  0

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_PIXELS_ERROR,
    READ_PADDING_ERRO,
    READ_INVALID_DATA,
    READ_INVALID_HEADER
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum read_status from_bmp(FILE* in, struct image* image);
enum write_status to_bmp(FILE* out, const struct image* image);

#endif
