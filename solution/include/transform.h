#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "image.h"

struct image rotate( struct image const source );

#endif
