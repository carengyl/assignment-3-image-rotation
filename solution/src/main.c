#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/transform.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

# define ARGC_REQUIRED 4
# define QUARTER 90

int main(int argc, char *argv[]) {
    
    if (argc != ARGC_REQUIRED) {
        perror("Error: ./image-transformer <source-image> <transformed-image> <angle>\n");
        return 1;
    }

    char *input_image = argv[1];
    char *output_image = argv[2];
    int angle = atoi(argv[3]);

    if (angle % QUARTER != 0 || abs(angle) > (3 * QUARTER) || (angle == 0 && strcmp(argv[3], "0") != 0)) {
        perror("The angle should be in {-270; -180; -90; 0; 90; 180; 270}\n");
        return 1;
    }

    struct image image = {0};
    FILE *input_file = fopen(input_image, "rb");

    if (!input_file) {
        perror("Invalid input file\n");

        if (image.data)
        {
            free(image.data);
            image.data = NULL;
        }

        return 1;
    }

    enum read_status read_status = from_bmp(input_file, &image);
    fclose(input_file);

    if (read_status != READ_OK) {
        perror("Can't read image\n");

        if (image.data)
        {
            free(image.data);
            image.data = NULL;
        }

        return 1;
    }

    if (angle != 0) {
        int rotations = (abs(angle) % (4 * QUARTER))/QUARTER;
        if (angle > 0) {
            rotations = 4 - rotations;
        }
        for (int i = 0; i < rotations; i++) {
            struct image rotated_image = rotate(image);

            if (image.data != NULL)
            {
                free(image.data);
                image.data = NULL;
            }

            image = rotated_image;
        }
    }

    FILE *output_file = fopen(output_image, "wb");

    if (!output_file) {
        perror("Invalid output file\n");
        
        if (image.data)
        {
            free(image.data);
            image.data = NULL;
        }
        
        return 1;
    }

    enum write_status write_status = to_bmp(output_file, &image);
    fclose(output_file);

    if (image.data)
    {
        free(image.data);
        image.data = NULL;
    }

    if (write_status != WRITE_OK) {
        perror("Can't write rotated image\n");
        return 1;
    } else {
        printf("Successfully rotated image\n");
    }

    return 0;
}
