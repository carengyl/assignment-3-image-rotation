#include "../include/bmp.h"

#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define BMP_HEADER_SIZE sizeof(struct bmp_header)
#define BYTE_ALIGNMENT 3
#define PADDING_SIZE 4

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static bool read_header(FILE *in, struct bmp_header *const header) {
    bool status = fread(header, BMP_HEADER_SIZE, 1, in);
    fseek(in, (long) 0, SEEK_CUR);

    if (!status) return false;
    fseek(in, (long) header->bOffBits, SEEK_SET);
    return true;
}

static size_t calculate_padding(size_t row_size) {
    return ((row_size + BYTE_ALIGNMENT)/PADDING_SIZE)*PADDING_SIZE - row_size;
}

static bool read_data(FILE *in, const struct image *const image) {
    size_t byte_width = PIXEL_SIZE * (image->width);
    size_t padding = calculate_padding(byte_width);

    struct pixel *start = image->data;
    
    for (size_t row_index = 0; row_index < image->height; ++row_index) {
        struct pixel *  row = start + row_index * image->width;

        if (fread(row, PIXEL_SIZE, image->width, in) != image->width) {
            return false;
        }

        fseek(in, (long)padding, SEEK_CUR);
    }

    return true;
}

enum read_status from_bmp(FILE* in, struct image* image) {
    struct bmp_header header;
    
    if (!read_header(in, &header)) {
        return READ_INVALID_SIGNATURE;
    }
    
    *image = create_image(header.biWidth, header.biHeight);

    if (!read_data(in, image)) {
        return READ_INVALID_DATA;
    }

    return READ_OK;
}

static bool write_header(FILE *out, struct image const *image) {
    struct bmp_header header;
    size_t row_size = PIXEL_SIZE * image->width + calculate_padding(PIXEL_SIZE * image->width);
    size_t image_size = row_size * image->height;

    header.bfType = BMP_BF_TYPE;
    header.bfileSize = image_size + BMP_BO_OFF_BITS;
    header.bfReserved = BMP_BF_RESERVED;
    header.bOffBits = BMP_BO_OFF_BITS;
    header.biSize = BMP_HEADER_SIZE - BMP_FILE_HEADER_SIZE;
    header.biWidth = image->width;
    header.biHeight = image->height;
    header.biPlanes = BMP_BI_PLANES;
    header.biBitCount = BMP_BI_BIT_COUNT;
    header.biCompression = BMP_BI_COMPRESSION;
    header.biSizeImage = image_size;
    header.biXPelsPerMeter = BMP_BI_X_PELS_PER_METER;
    header.biYPelsPerMeter = BMP_BI_Y_PELS_PER_METER;
    header.biClrUsed = BMP_BI_CLR_USED;
    header.biClrImportant = BMP_BI_CLR_IMPORTANT;

    if (!fwrite(&header, BMP_HEADER_SIZE, 1, out)) {
        return false;
    }
    return true;
}

static void write_data(FILE *out, struct image const *image) {
    size_t padding = calculate_padding(PIXEL_SIZE * image->width);
    for (size_t row_number = 0; row_number < image->height; ++row_number) {
        const struct pixel *current_row = image->data + row_number * image->width;
        fwrite(current_row, PIXEL_SIZE, image->width, out);
        uint8_t padding_bytes[BYTE_ALIGNMENT] = {0};
        fwrite(&padding_bytes, 1, padding, out);
    }
}

enum write_status to_bmp(FILE* out, const struct image* image) {
    if (!write_header(out, image)) {
        return WRITE_ERROR;
    }

    write_data(out, image);

    return WRITE_OK;
}
