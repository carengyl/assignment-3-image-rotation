#include "../include/image.h"
#include "../include/transform.h"

#include <stdlib.h>


struct image rotate(struct image const source) {
    struct image rotated_image = create_image(source.height, source.width);
    
    for (size_t source_y = 0; source_y < source.height; source_y++) {
        for (size_t source_x = 0; source_x < source.width; source_x++) {
            *image_at(&rotated_image, source.height - source_y - 1, source_x) = *image_at(&source, source_x, source_y);
        }
    }

    return rotated_image;
}
