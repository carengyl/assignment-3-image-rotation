#include "../include/image.h"

#include <stdlib.h>

struct image create_image(uint64_t width, uint64_t height) {
    return (struct image) {
        .width = width,
        .height = height,
        .data = malloc(width * height * PIXEL_SIZE)
    };
}

void free_image(struct image* image) {
    free(image->data);
}

struct pixel *image_at(const struct image *const image, size_t x, size_t y) {
    return image->data + x + y * image->width;
}
